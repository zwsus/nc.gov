<?php

/**
 * OrderStatus frontend controller
 *
 * @author Mohammad Z Rahman
 */
class Zws_OrderStatus_IndexController extends Mage_Core_Controller_Front_Action
{

    /**
     * Index action
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Method to handle post action and display outout
     *
     * @throws Exception
     */
    public function viewAction()
    {
        $post = $this->getRequest()->getPost();
        if (!$post) {
            return $this->_forward('noRoute');
        }

        try {
            $helper = Mage::helper('zws_orderstatus');
            $session = Mage::getModel('core/session');
            $session->setFormData($post);

            // Form validation
            $this->_view($post);

            // Find order in the database
            $order = Mage::getModel('sales/order')->loadByIncrementId($post['orderId']);
            if ($order->getEntityId() === null) {
                throw new Exception($helper->__('Invalid Order Id. Please try again'));
            }

            // Match the email with order email.
            if ($order->getCustomerEmail() != $post['email']) {
                throw new Exception($helper->__('Email does not match with the order number.'));
            }

            // Clean session
            $session->unsFormData();

            // Set data in block and render
            $this->loadLayout();
            $this->getLayout()->getBlock('zws.orderstatus.view')->setOrder($order);
            $this->renderLayout();
        } catch (Exception $ex) {
            $session->addError($ex->getMessage());
            $this->_redirect('*/*/');
        }
    }

    /**
     * This is a supporting method for the viewAction()
     */
    private function _view($post)
    {
        $helper = Mage::helper('zws_orderstatus');
        
        // Make sure order id is not empty
        if (!Zend_Validate::is(trim($post['orderId']), 'NotEmpty')) {
            throw new Exception($helper->__('Order ID is required'));
        }

        // Make sure email is not empty
        if (!Zend_Validate::is(trim($post['email']), 'NotEmpty')) {
            throw new Exception($helper->__('Email is required'));
        }
    }

}
